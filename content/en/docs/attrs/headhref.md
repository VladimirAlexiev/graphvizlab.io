---
defaults:
- '""'
flags:
- map
- svg
minimums: []
title: headhref
types:
- escString
used_by: E
---
Synonym for [`headURL`]({{< ref "headURL.md" >}}).
