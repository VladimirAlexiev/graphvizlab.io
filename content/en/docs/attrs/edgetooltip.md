---
defaults:
- '""'
flags:
- cmap
- svg
minimums: []
title: edgetooltip
types:
- escString
used_by: E
---
Tooltip annotation attached to the non-label part of an edge.
