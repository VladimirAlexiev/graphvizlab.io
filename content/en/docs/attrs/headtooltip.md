---
defaults:
- '""'
flags:
- cmap
- svg
minimums: []
title: headtooltip
types:
- escString
used_by: E
---
Tooltip annotation attached to the head of an edge.

Used only if the edge has a [`headURL`]({{< ref "headURL.md" >}}) attribute.
