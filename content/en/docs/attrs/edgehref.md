---
defaults:
- '""'
flags:
- map
- svg
minimums: []
title: edgehref
types:
- escString
used_by: E
---
Synonym for [`edgeURL`]({{< ref "edgeURL.md" >}}).
