---
defaults:
- '""'
flags:
- cmap
- svg
minimums: []
title: labeltooltip
types:
- escString
used_by: E
---
Tooltip annotation attached to label of an edge.
