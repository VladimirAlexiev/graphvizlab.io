---
defaults:
- '""'
flags:
- map
- svg
minimums: []
title: labelURL
types:
- escString
used_by: E
---
If defined, `labelURL` is the link used for the label of an edge.

`labelURL` overrides any [`URL`]({{< ref "URL.md" >}}) defined for the edge.
