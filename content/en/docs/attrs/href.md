---
defaults:
- '""'
flags:
- map
- postscript
- svg
minimums: []
title: href
types:
- escString
used_by: GCNE
---
Synonym for [`URL`]({{< ref "URL.md" >}}).
