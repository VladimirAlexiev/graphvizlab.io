---
defaults:
- '""'
flags:
- map
- svg
minimums: []
title: tailhref
types:
- escString
used_by: E
---
Synonym for [`tailURL`]({{< ref "tailURL.md" >}}).
