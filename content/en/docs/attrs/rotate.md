---
defaults:
- '0'
flags: []
minimums: []
title: rotate
types:
- int
used_by: G
---
If `rotate=90`, sets drawing orientation to landscape.
