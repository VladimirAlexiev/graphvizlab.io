---
defaults:
- '""'
flags:
- map
- svg
minimums: []
title: headURL
types:
- escString
used_by: E
---
If defined, `headURL` is output as part of the head label of the edge.

Also, this value is used near the head node, overriding any [`URL`]({{< ref "URL.md" >}}) value.

See [limitation]({{< ref "_index.md#undir_note" >}}).
