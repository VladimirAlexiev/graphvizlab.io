---
defaults:
- '""'
flags:
- map
- svg
minimums: []
title: labelhref
types:
- escString
used_by: E
---
Synonym for [`labelURL`]({{< ref "labelURL.md" >}}).
